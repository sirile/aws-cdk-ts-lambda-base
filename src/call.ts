import type { APIGatewayProxyHandler } from 'aws-lambda'
import axios, { AxiosResponse } from 'axios'

/**
 * Example of doing an external call and returning the result.
 *
 * @param event The event forwarded by the Lambda
 */

interface UuidResponse {
  uuid: string
}

export const call: APIGatewayProxyHandler = async (event) => {
  console.debug(`Call to Call-function. Event: ${JSON.stringify(event)}`)
  const response = (await axios(`https://httpbin.org/uuid`)) as AxiosResponse<UuidResponse>
  return {
    statusCode: 200,
    body: `Got a result: ${response.data.uuid}`,
  }
}
