import type { APIGatewayProxyHandler } from 'aws-lambda'

/**
 * As this is a very simple example handler, there is no real use for the async. Specifying it means
 * that the return value is automatically a promise, though, which helps keep the code clean. The
 * ESLint disable for the first line can be removed when there's an actual implementation
 *
 * @param event The event forwarded by the Lambda
 */

// eslint-disable-next-line @typescript-eslint/require-await
export const hello: APIGatewayProxyHandler = async (event) => {
  console.debug(`Call to Hello-function. Event: ${JSON.stringify(event)}`)
  return {
    statusCode: 200,
    body: JSON.stringify({ message: `Hello, ${event.queryStringParameters?.name as string}` }),
  }
}
