import { Configuration } from 'webpack'
// eslint-disable-next-line unicorn/import-style,unicorn/prefer-node-protocol
import { resolve } from 'path'

const config: Configuration = {
  entry: { hello: './src/hello.ts', call: './src/call.ts' },
  output: {
    filename: '[name].js',
    libraryTarget: 'commonjs2',
    // eslint-disable-next-line unicorn/prefer-module
    path: resolve(__dirname, 'build'),
  },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader' }],
  },
  resolve: {
    extensions: ['.js', '.ts'],
  },
  target: 'node',
  mode: process.env.NODE_ENV === 'dev' ? 'development' : 'production',
  devtool: 'inline-source-map',
}

// eslint-disable-next-line import/no-default-export
export default config
