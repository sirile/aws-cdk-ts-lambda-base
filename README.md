# General

This project is the base for TypeScript based Lambda development. It utilizes AWS CDK
(Cloud Development Kit) for setting up the AWS components.

## Features

- [TypeScript](https://www.typescriptlang.org/) support
- [AWS CDK](https://aws.amazon.com/cdk/) for modeling provisioning the cloud resources
- [WebPack 5](https://webpack.js.org/) used to minimize build size
- [Ava](https://github.com/avajs/ava) for testing
- [nyc](https://github.com/istanbuljs/nyc) and [Istanbul](https://istanbul.js.org/) for coverage
- [ESLint](https://eslint.org/) with [AirBnB-base-typescript](https://github.com/iamturns/eslint-config-airbnb-typescript) for linting
- [Prettier](https://prettier.io/) for code formatting
- [Yarn](https://classic.yarnpkg.com/lang/en/) for package management
- [husky](https://github.com/typicode/husky) with [pretty-quick](https://prettier.io/docs/en/precommit.html) for pre-commit hook
- Support for debugging in VSCode Studio via launch settings

## TODO

- CI/CD pipeline

## Components and prerequisites

### AWS CDK (Cloud Development Kit)

CDK generates CloudFormation scripts and is responsible for maintaining the
status of the stacks in the defined account.

On Mac AWS CDK can be installed with `brew install aws-cdk`. It relies on aws-cli for configurations, so
installing and configuring that has to be done. More information [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html).

The main definition is _cdk.json_ file which tells the framework that the entrypoint
to the stack is _bin/<projectname>_. In this example the file is _bin/aws-cdk-ts-lambda-base.ts_. This file then loads the stack components from under the
_lib_-folder.

## Things to modify when starting a new function

Some of the definitions will come from ENV-variables when CI/CD is set up.

- package.json - Project name, description etc
- index.js - Add methods for the endpoints
- CDK configuration file names and class names etc
  - cdk.json
  - bin/aws-cdk-ts-lambda-base.ts
  - lib/aws-cdk-ts-lambda-base-stack.ts
  - lib/hello-service.ts
  - lib/call-service.ts
- Tests under test-folder
- webpack.config.js - Add possible whitelisted modules

## Open questions

- Does using nodeExternals() and whitelist give any advantage un bundle size?

## Use cases

The solution can be built and packaged with

```bash
yarn build
```

Unit tests against the handlers are run with

```bash
yarn test
```

Coverage report can be generated with

```bash
yarn coverage
```

The report can be opened with the command `open coverage/lcov-report/index.html`.

Local version of the application can be started with

```bash
yarn start
```

Starting the application in watch mode for quick development is done with

```bash
yarn watch
```

### Managing the deployment in the cloud

When starting to use the account for the application, it needs to be bootstrapped. It only needs to
be done once by running

```bash
yarn bootstrap
```

Solution can be deployed with the current AWS credentials with

```bash
yarn deploy
```

Tearing down of the application can be done with

```bash
yarn destroy
```

### Code maintaining

Linting the code is done with

```bash
yarn lint
```

Upgrading packages interactively to latest versions is done with

```bash
yarn latest
```
