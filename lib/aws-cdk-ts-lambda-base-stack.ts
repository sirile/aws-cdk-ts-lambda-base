import * as cdk from '@aws-cdk/core'
import { RestApi } from '@aws-cdk/aws-apigateway'
import { HelloService } from './hello-service'
import { CallService } from './call-service'

export class AwsCdkTsLambdaBaseStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)

    // Create the API-GW and pass it along to the services
    const api = new RestApi(this, 'hello-api', {
      restApiName: 'Base Service',
      description: 'This service is an example.',
    })

    // Services are defined here
    new HelloService(this, 'Hello', api)
    new CallService(this, 'Call', api)
  }
}
