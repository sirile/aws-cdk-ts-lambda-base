import { Construct } from '@aws-cdk/core'
import { RestApi, LambdaIntegration } from '@aws-cdk/aws-apigateway'
import * as Lambda from '@aws-cdk/aws-lambda'

export class CallService extends Construct {
  constructor(scope: Construct, id: string, api: RestApi) {
    super(scope, id)

    const callHandler = new Lambda.Function(this, 'CallHandler', {
      runtime: Lambda.Runtime.NODEJS_12_X,
      code: Lambda.Code.asset('build'),
      handler: 'call.call',
    })

    const getCallIntegration = new LambdaIntegration(callHandler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' },
    })

    api.root.addResource('call').addResource('v1').addMethod('GET', getCallIntegration)
  }
}
