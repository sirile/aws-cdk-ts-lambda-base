import { Construct } from '@aws-cdk/core'
import { RestApi, LambdaIntegration } from '@aws-cdk/aws-apigateway'
import * as Lambda from '@aws-cdk/aws-lambda'

export class HelloService extends Construct {
  constructor(scope: Construct, id: string, api: RestApi) {
    super(scope, id)

    const helloHandler = new Lambda.Function(this, 'HelloHandler', {
      runtime: Lambda.Runtime.NODEJS_12_X,
      code: Lambda.Code.asset('build'),
      handler: 'hello.hello',
    })

    const getHelloIntegration = new LambdaIntegration(helloHandler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' },
    })

    api.root.addResource('hello').addResource('v1').addMethod('GET', getHelloIntegration)
  }
}
