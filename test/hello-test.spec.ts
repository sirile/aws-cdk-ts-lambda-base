import type { Context, Callback, APIGatewayProxyEvent } from 'aws-lambda'
import test from 'ava'
import { hello } from '../src/hello'

// Test event
const helloEvent: Partial<APIGatewayProxyEvent> = {
  queryStringParameters: { name: 'Ilkka' },
}

// Tests for the methods
test('Call hello and check result', async (t) => {
  const result = await hello(helloEvent as APIGatewayProxyEvent, {} as Context, {} as Callback)
  t.deepEqual(result, {
    statusCode: 200,
    body: `{"message":"Hello, ${helloEvent.queryStringParameters?.name as string}"}`,
  })
})

test('Call hello with an empty request', async (t) => {
  const result = await hello({} as APIGatewayProxyEvent, {} as Context, {} as Callback)
  t.deepEqual(result, { statusCode: 200, body: `{"message":"Hello, undefined"}` })
})
