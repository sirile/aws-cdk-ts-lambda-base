import type { Context, Callback, APIGatewayProxyEvent } from 'aws-lambda'
import test from 'ava'
import * as nock from 'nock'
import { call } from '../src/call'

const uuid = 'f511b17d-15f8-4286-b285-a099eab31186'

test('Call external service and check the result', async (t) => {
  nock('https://httpbin.org').get(`/uuid`).reply(200, { uuid })
  const result = await call({} as APIGatewayProxyEvent, {} as Context, {} as Callback)
  if (result) {
    t.is(result.statusCode, 200)
    t.is(result.body, `Got a result: ${uuid}`)
  } else {
    t.fail()
  }
})
