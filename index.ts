import * as Koa from 'koa'
import * as Router from 'koa-router'
import * as logger from 'koa-logger'
import type { APIGatewayProxyEvent, Context, Callback, APIGatewayProxyResult } from 'aws-lambda'
import { hello } from './src/hello'
import { call } from './src/call'

// Boilerplate for faking the API-GW locally
const app = new Koa()
app.use(logger())
const router = new Router()

function createEvent(ctx: Koa.DefaultContext): APIGatewayProxyEvent {
  const event: Partial<APIGatewayProxyEvent> = {
    queryStringParameters: ctx.query as { [name: string]: string },
    body: ctx.body as string,
  }
  return event as APIGatewayProxyEvent
}

function createResponse(result: APIGatewayProxyResult | void, ctx: Koa.DefaultContext): void {
  if (result) {
    ctx.body = result.body
    ctx.status = result.statusCode
  }
}

// Functions are defined here
router.get('/prod/hello/v1', async (ctx) => {
  createResponse(await hello(createEvent(ctx), {} as Context, {} as Callback), ctx)
})
router.get('/prod/call/v1', async (ctx) => {
  createResponse(await call(createEvent(ctx), {} as Context, {} as Callback), ctx)
})

// Start the server
app.use(router.routes())
app.listen(3000)
