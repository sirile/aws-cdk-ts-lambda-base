#!/usr/bin/env node
import * as cdk from '@aws-cdk/core'
import { AwsCdkTsLambdaBaseStack } from '../lib/aws-cdk-ts-lambda-base-stack'

const app = new cdk.App()

new AwsCdkTsLambdaBaseStack(app, 'AwsCdkTsLambdaBaseStack')
